PROGS := ft spec

.PHONY: all
all: $(PROGS)

.PHONY: clean
clean:
	rm -f $(PROGS)

ft: ft.cpp
	g++ -o $@ -std=c++14 $^

spec: spec.cpp
	g++ -o $@ -std=c++14 $^ -lSDL2 -lSDL2_mixer -lepoxy
