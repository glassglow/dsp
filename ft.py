import cmath
import math
import sys

# Arguments are samples at evenly-spaced intervals
if len(sys.argv) < 2:
    raise TypeError('missing argument(s): signal samples')
x = [float(x) for x in sys.argv[1:]]
N = len(x)

# Transform output
X = []

# Transform algorithm
for k in range(0, N):

    # Output is a complex number
    X.append(complex(0, 0))

    # Perform sum operation
    for n in range(0, N):
        X[k] += x[n] * cmath.exp(1j * -2 * math.pi * k * n / N)

    # Divide by N so that each frequency contributes the same amount
    X[k] /= N

    # Round to nearest 2 digits
    re = round(X[k].real, 2)
    im = round(X[k].imag, 2)

    X[k] = complex(re, im)

# Output transform results
for k in range(0, N):
    A = math.sqrt(X[k].real * X[k].real + X[k].imag * X[k].imag)
    P = math.degrees(math.atan2(X[k].imag, X[k].real))

    print(k, 'Hz:')
    print('  Real:     ', X[k].real)
    print('  Imaginary:', X[k].imag)
    print('  Amplitude:', A)
    print('  Phase:    ', P)
