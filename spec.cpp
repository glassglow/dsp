#include <epoxy/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <algorithm>
#include <complex>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <cstdint>

// Utilities

std::vector<double> normalize_s16(void *stream, int stream_len) {
  std::uint8_t *bytes = static_cast<std::uint8_t *>(stream);

  std::vector<double> samples;

  for(int i = 0; i < stream_len; i += sizeof(std::int16_t)) {
    std::int16_t sample = *reinterpret_cast<std::int16_t *>(bytes + i);
    samples.push_back(sample / (double) INT16_MAX);
  }

  return samples;
}

// Fourier transforms

std::vector<std::complex<double>> dft(std::vector<double> x) {
  int N = x.size();

  std::vector<std::complex<double>> X;
  std::complex<double> i(0, 1);

  for(int k = 0; k < N; k += 1) {
    X.push_back(std::complex<double>(0, 0));

    for(int n = 0; n < N; n += 1) {
      X[k] += x[n] * std::exp(i * -2.0 * M_PI *
          static_cast<double>(k) * static_cast<double>(n) /
          static_cast<double>(N));
    }
  }

  return X;
}

// FFT algorithm lifted pretty much wholesale from
// http://www.dspguide.com/ch12/3.htm

std::vector<std::complex<double>> fft(std::vector<double> samples) {
  // Copy real-valued signal into complex array
  std::vector<std::complex<double>> x;
  for(auto it = samples.begin(); it != samples.end(); ++it) {
    x.push_back(std::complex<double>(*it, 0));
  }

  int N = x.size();

  // Do bit reversal sorting
  int j = N / 2;
  for(int i = 1; i < N - 1; i += 1) {
    // Swap
    if(i < j) {
      std::complex<double> temp = samples[j];
      x[j] = samples[i];
      x[i] = temp;
    }

    // Magic???
    int k = N / 2;
    while(k <= j) {
      j -= k;
      k /= 2;
    }
    j += k;
  }

  // FFT butterflies
  int M = std::log2(N);

  // Synthesis loop
  for(int i = 1; i <= M; i += 1) {
    double le = std::pow(2, i);
    double le2 = le / 2;

    std::complex<double> u(1, 0);
    std::complex<double> s(std::cos(M_PI / le2), -std::sin(M_PI / le2));

    // Sub-DFTs
    for(int j = 1; j <= le2; j += 1) {

      // Individual butterflies
      for(int k = j - 1; k <= N - 1; k += le) {
        int kp = k + le2;

        std::complex<double> t;
        t.real(x[kp].real() * u.real() - x[kp].imag() * u.imag());
        t.imag(x[kp].real() * u.imag() + x[kp].imag() * u.real());

        x[kp] = x[k] - t;
        x[k] += t;
      }

      double tr = u.real();
      double ti = u.imag();
      u.real(tr * s.real() - ti * s.imag());
      u.imag(tr * s.imag() + ti * s.real());
    }
  }

  return x;
}

// High-level stats collector

struct MusicStats {
  std::vector<double> frequencies;
  double lo_amp;
  double hi_amp;
  double av_amp;
};

void update_stats_from_fft(MusicStats *stats, std::vector<double> samples) {
  stats->frequencies.clear();
  stats->lo_amp = 0.0;
  stats->hi_amp = 0.0;
  stats->av_amp = 0.0;

  std::vector<std::complex<double>> frequencies = fft(samples);

  int bucket = 0;
  int bucket_size = 8;

  double amplitude = 0.0;
  for(auto it = frequencies.begin(); it != frequencies.end(); ++it) {
    amplitude += std::sqrt(it->real() * it->real() + it->imag() * it->imag());

    stats->lo_amp = std::min(stats->lo_amp, amplitude);
    stats->hi_amp = std::max(stats->hi_amp, amplitude);
    stats->av_amp += amplitude;

    bucket += 1;
    if(bucket == bucket_size) {
      stats->frequencies.push_back(amplitude);
      amplitude = 0.0;
      bucket = 0;
    }
  }

  stats->av_amp /= frequencies.size();
}

// Postprocessors

void print_samples(int channel, void *stream, int stream_len, void *data) {
  std::cout << "PROCESSOR CALLED: print_samples" << std::endl;
  std::cout << "  Sample Count: " << stream_len << std::endl;

  std::int16_t *samples = static_cast<std::int16_t *>(stream);
  std::cout << "  First 10 Samples: ";
  for(int i = 0; i < 10 && i < stream_len; i += 1) {
    std::cout << samples[i] << " ";
  }
  std::cout << std::endl;
}

void print_normalized_samples(int channel, void *stream, int stream_len, void *data) {
  std::cout << "PROCESSOR CALLED: print_normalized_samples" << std::endl;
  std::cout << "  Sample Count: " << stream_len << std::endl;

  std::vector<double> samples = normalize_s16(stream, stream_len);
  std::cout << "  First 10 Samples: ";
  for(int i = 0; i < 10 && i < stream_len; i += 1) {
    std::cout << samples[i] << " ";
  }
  std::cout << std::endl;
}

void print_dft(int channel, void *stream, int stream_len, void *data) {
  std::cout << "PROCESSOR CALLED: print_dft" << std::endl;
  std::cout << "  Sample Count: " << stream_len << std::endl;

  std::vector<double> samples = normalize_s16(stream, stream_len);
  std::vector<std::complex<double>> freqs = dft(samples);

  std::cout << "  Frequencies 435 Hz - 445 Hz: ";
  for(int i = 435; i <= 445; i += 1) {
    std::cout << freqs[i] << " ";
  }
  std::cout << std::endl;
}

void print_fft(int channel, void *stream, int stream_len, void *data) {
  std::cout << "PROCESSOR CALLED: print_fft" << std::endl;
  std::cout << "  Sample Count: " << stream_len << std::endl;

  std::vector<double> samples = normalize_s16(stream, stream_len);
  std::vector<std::complex<double>> freqs = fft(samples);

  std::cout << "  Frequencies 435 Hz - 445 Hz: ";
  for(int i = 435; i <= 445; i += 1) {
    std::cout << freqs[i] << " ";
  }
  std::cout << std::endl;
}

void analyze_samples(int channel, void *stream, int stream_len, void *data) {
  MusicStats *stats = static_cast<MusicStats *>(data);

  std::vector<double> samples = normalize_s16(stream, stream_len);
  update_stats_from_fft(stats, samples);
}

// Simple particle system

struct RGB {
  double r;
  double g;
  double b;
};

struct HSL {
  double h;
  double s;
  double l;
};

// HSL to RGB algorithm from
// http://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion

double hue_to_rgb(double p, double q, double t) {
  if(t < 0.0) t += 1.0;
  if(t > 1.0) t -= 1.0;

  if(t * 6.0 < 1.0) return p + (q - p) * 6.0 * t;
  if(t * 2.0 < 1.0) return q;
  if(t * 3.0 < 2.0) return p + (q - p) * (2.0 / 3.0 - t) * 6.0;

  return p;
}

RGB hsl_as_rgb(HSL hsl) {
  double q = hsl.l < 0.5 ? hsl.l * (1.0 + hsl.s) : hsl.l + hsl.s - hsl.l * hsl.s;
  double p = 2.0 * hsl.l - q;

  RGB rgb;
  rgb.r = hue_to_rgb(p, q, hsl.h + 1.0 / 3.0);
  rgb.g = hue_to_rgb(p, q, hsl.h);
  rgb.b = hue_to_rgb(p, q, hsl.h - 1.0 / 3.0);

  return rgb;
}

class ParticleSystem {
  static constexpr double GX = 0.0;
  static constexpr double GY = 0.0;

  struct Particle {
    double px;
    double py;
    double vx;
    double vy;
    double gx;
    double gy;

    RGB rgb;

    double time;
    double ttl;
  };

  std::vector<Particle> _particles;
  double _x;
  double _y;
  double _r;

  public:
    ParticleSystem(double x, double y, double r)
      :_x(x), _y(y), _r(r) {
    }

    void spawn(double angle, double energy, int amount) {
      angle += std::rand() % 5;

      double radians = angle * M_PI / 180.0;
      double radius = _r + 100 * energy;

      for(int i = 0; i < amount; ++i) {
        Particle p;

        p.px = _x + radius * std::cos(radians);
        p.py = _y + radius * std::sin(radians);

        p.vx = 500.0 * std::sqrt(10 * energy) * std::cos(radians);
        p.vy = 500.0 * std::sqrt(10 * energy) * std::sin(radians);

        p.gx = -400.0 * (1 - 10 * energy) * std::cos(radians);
        p.gy = -400.0 * (1 - 10 * energy) * std::sin(radians);

        HSL hsl;
        hsl.h = angle / 270.0;
        hsl.s = 1.0 - energy;
        hsl.l = 0.05 + std::sqrt(energy);

        p.rgb = hsl_as_rgb(hsl);

        p.time = 0.0;
        //p.ttl = std::log(100 * energy) / 4.0;
        p.ttl = 1.0 - energy;

        _particles.push_back(p);
      }
    }

    void update(double dt) {
      for(auto it = _particles.begin(); it != _particles.end(); ++it) {
        it->time += dt;

        it->px += it->vx * dt;
        it->py += it->vy * dt;

        it->vx += it->gx * dt;
        it->vy += it->gy * dt;
      }

      _particles.erase(std::remove_if(_particles.begin(), _particles.end(),
            [](Particle &p) { return p.time >= p.ttl; }),
          _particles.end());
    }

    void draw() {
      float old_size;
      glGetFloatv(GL_POINT_SIZE, &old_size);

      glPointSize(10.0);
      glBegin(GL_POINTS);

      for(auto it = _particles.begin(); it != _particles.end(); ++it) {
        double alpha = it->time / it->ttl;
        double beta = 1.0 - alpha;

        glColor3f(it->rgb.r * beta, it->rgb.g * beta, it->rgb.b * beta);
        glVertex3f(it->px, it->py, 0.0f);
      }

      glEnd();
      glPointSize(old_size);
    }

};

// Main code

int main(int argc, char **argv) {
  // Parse command line arguments
  std::vector<std::string> args(argv + 1, argv + argc);
  std::string songpath = "";

  for(std::string arg: args) {
    if(songpath == "") {
      songpath = arg;
    } else {
      std::cerr << "Warning: too many arguments" << std::endl;
    }
  }

  if(songpath == "") {
    std::cerr << "Missing argument: song file" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Initialize SDL & plugins
  if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    std::cerr << "Failed to initialize SDL" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if(Mix_Init(MIX_INIT_OGG | MIX_INIT_MP3) < 0) {
    std::cerr << "Failed to initialize support for MP3 and OGG" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
    std::cerr << "Failed to intialize SDL_mixer" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Create window & GL context
  SDL_Window *window = SDL_CreateWindow("Song Analyzer",
      SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720,
      SDL_WINDOW_OPENGL);
  if(window == nullptr) {
    std::cerr << "Failed to create SDL window" << std::endl;
    std::exit(EXIT_FAILURE);
  }
  
  SDL_GLContext gl_ctx = SDL_GL_CreateContext(window);
  if(gl_ctx == nullptr) {
    std::cerr << "Failed to create SDL GL context" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Register "effects" (which we're actually using to record samples as
  // they're streamed)
  MusicStats stats;

  if(Mix_RegisterEffect(MIX_CHANNEL_POST, analyze_samples, nullptr, &stats) == 0) {
    std::cerr << "Failed to register post-processor" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Load music
  Mix_Music *song = Mix_LoadMUS(songpath.c_str());
  if(song == nullptr) {
    std::cerr << "Failed to load song from file " << songpath << std::endl;
    std::exit(EXIT_FAILURE);
  }

  // Set up GL stuff
  glViewport(0, 0, 1280, 720);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0f, 1280.0f, 720.0f, 0.0f, 0.0f, 1.0f);

  glMatrixMode(GL_MODELVIEW);

  // Set up particle system
  ParticleSystem particles(640.0, 360.0, 80.0);

  // Set up timer
  Uint64 freq = SDL_GetPerformanceFrequency();
  Uint64 then = SDL_GetPerformanceCounter();
  Uint64 now = then;
  double dt = 0.0;

  double particle_timer = 0.0;
  double particle_delay = 0.001;

  // Event loop
  SDL_Event e;
  bool running = true;

  while(running) {
    while(SDL_PollEvent(&e)) {
      switch(e.type) {
        case SDL_QUIT:
          running = false;
          break;

        case SDL_KEYDOWN:
          switch(e.key.keysym.sym) {
            case SDLK_SPACE:
              if(Mix_PlayingMusic() == 0) {
                Mix_PlayMusic(song, -1);
              } else {
                if(Mix_PausedMusic() == 0) {
                  Mix_PauseMusic();
                } else {
                  Mix_ResumeMusic();
                }
              }

              break;
          }

          break;
      }
    }

    // update timer
    dt += ((now - then)) / (double) freq;

    then = now;
    now = SDL_GetPerformanceCounter();

    while(dt >= 1 / 60.0) {
      dt -= 1 / 60.0;

      particle_timer += 1 / 60.0;
      if(particle_timer >= particle_delay) {
        particle_timer = 0.0;

        for(int i = 0; i < stats.frequencies.size(); ++i) {
          double energy = stats.frequencies[i] / stats.hi_amp;

          if(energy > 0.1 || energy < 0.005) {
            continue;
          }

          double angle = i / (double) stats.frequencies.size();
          angle *= -270.0;
          angle += 45.0;

          particles.spawn(angle, energy, 1);
        }
      }

      particles.update(1 / 60.0);
    }

    glClear(GL_COLOR_BUFFER_BIT);
    particles.draw();
    SDL_GL_SwapWindow(window);
  }

  // Clean up music
  Mix_FreeMusic(song);

  // Clean up window & GL context
  SDL_GL_DeleteContext(gl_ctx);
  SDL_DestroyWindow(window);

  // Shutdown
  Mix_Quit();
  SDL_Quit();
  return 0;
}
