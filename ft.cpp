#include <complex>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <cstdlib>

using namespace std;
using namespace std::complex_literals;

int main(int argc, char **argv) {
  vector<string> args(argv + 1, argv + argc);

  double N;
  vector<double> x;
  vector<complex<double>> X;

  for(string arg: args) {
    x.push_back(atof(arg.c_str()));
  }

  if(x.size() == 0) {
    cerr << "No signal to analyze" << endl;
  }

  N = x.size();

  for(int a = 0; a < x.size(); a += 1) {
    double k = a;
    X.push_back(0i);

    for(int b = 0; b < x.size(); b += 1) {
      double n = b;
      X[k] += x[n] * exp(1i * -2.0 * M_PI * k * n / N);
    }

    X[k] /= N;

    double re = X[k].real();
    double im = X[k].imag();

    re = round(100 * re) / 100.0;
    im = round(100 * im) / 100.0;

    X[k].real(re);
    X[k].imag(im);
  }

  for(int k = 0; k < x.size(); k += 1) {
    double A = sqrt(X[k].real() * X[k].real() + X[k].imag() * X[k].imag());
    double P = atan2(X[k].imag(), X[k].real()) * 180.0 / M_PI;

    cout << k << " Hz:" << endl;
    cout << "  Real:      " << X[k].real() << endl;
    cout << "  Imaginary: " << X[k].imag() << endl;
    cout << "  Amplitude: " << A << endl;
    cout << "  Phase:     " << P << endl;
  }
}
